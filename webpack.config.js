const path=require('path')
const HtmlWebpackplugin=require('html-webpack-plugin')

module.exports={
    entry:'./src/index.js',
    output:{
        path:path.join(__dirname, '/dist'),
        filename:'index_bundle.js'
    },
    module: {
        rules: [
            {
                test:/\.js$/,
                exclude:/node_modules/,
                use:{
                    loader:'babel-loader',
                }
            },
            {
                test: /\.css$/,
                exclude: /node_modules/,
                use: [
                  {
                    loader: 'style-loader',
                  },
                  {
                    loader: 'css-loader'
                  },
                ]
            }
        ]
    },
    plugins:[
        new HtmlWebpackplugin(
            {
            template:'./src/index.html'
            }
            )
    ]
}