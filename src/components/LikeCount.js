import React, { Component } from 'react';
import './LikeCount.css';
class LikeCount extends Component{
    constructor(props)
    {
        super(props)
    }
    render(){
        let count=this.props.count;
        if(this.props.status)
        count++;
        return (
            <div>
                <div className="likes">
                <h5>{count} likes</h5>
                </div>
            </div>
        )
    }
}
export default LikeCount;