import React, { Component } from 'react';
import './PostedImage.css'
class PostedImage extends Component{
render(){
    return(
        <div>
            <div className="posted-image">
            <img src={this.props.image}></img>
            </div>
        </div>
    )
}
}
export default PostedImage;