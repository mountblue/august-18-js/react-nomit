import React, { Component } from 'react';
import './CommentBox.css'

class CommentBox extends Component{
    constructor(props)
    {
       super(props)
       this.state={text:'',commentList:[]}
    }
    makeComment(event)
    {  
      this.setState({text:event.target.value}) 
    }
    isCommented(event)
    {
        let list=this.state.commentList
        if(event.target.value!='')
        {
            list.push({userName:this.props.user,comment:event.target.value})
            this.props.update(this.props.id,'input',event.target.value)
        }
        this.setState({text:'',commentList:list})
    }
    render()
    {
        let personalComment=[]
        if(this.state.commentList.length&&!this.props.status)
        personalComment=this.state.commentList.map((list,i)=>{
            return <li key={i}><label>{list.userName} </label>{list.comment}</li>
        })
        else
        personalComment=[]
        return(
            <div className="comment-input">
            <ul>{personalComment}</ul>
            <input type="text" onChange={this.makeComment.bind(this)} onClick={this.isCommented.bind(this)} value={this.state.text} ></input>
            </div>
        ) 
    }
 }

export default CommentBox;