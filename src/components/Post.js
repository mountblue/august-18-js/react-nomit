import React, { Component } from 'react';
import Title from './Title.js';
import PostedImage from './PostedImage.js';
import SocialIcons from './SocialIcons.js';
import LikeCount from './LikeCount.js';
import CommentList from './CommentList.js';
import CommentBox from './CommentBox.js';
import './Post.css';


class Post extends Component{
    constructor(props)
    {
        super(props)
        this.state={postItems:this.props.postItems};
    }
    // updatePostItems(key,name,value){
    //     postList=this.props.postItems.map((post,i)=>{
    //             if(i===key)
    //             post.name=value
    //             return post
    //     })
    //      this.props.update(postList)
    // }
    updatePostItems(key,name,status)
    { 
        let posts;
        posts=this.state.postItems.map((post,i)=>{
            if(i===key)
            {
                if(name==='like')
                post.like=status;
                if(name==='comment')
                post.comment=status;
                if(name==='save')
                post.save=status;
                if(name==='input')
                {
                post.commentList.push({userName:post.user,comment:status})
                }
            }
            return post
        })
        this.setState({postItems:posts})
    }
    render(){
        let posts;
        posts=this.state.postItems.map((post,i)=>{
            return(
                <li className="recent-post" key={i}>
                    <Title  id={i} image={post.profileImg} name={post.profileName}/>
                    <PostedImage id={i} image={post.postImg}/>
                    <SocialIcons id={i} like={post.like} comment={post.commentt} save={post.save}  update={this.updatePostItems.bind(this)}/>
                    <LikeCount id={i} count={post.likesCount} status={post.like} />
                    <CommentList id={i} list={post.commentList} status={post.comment} />
                    <CommentBox id={i} status={post.comment} update={this.updatePostItems.bind(this)} user={post.user}/>
               </li>
            )
        })
        return(
            <div>
                <ul>{posts}</ul>
                
            </div>
        )

    }
}
export default Post;