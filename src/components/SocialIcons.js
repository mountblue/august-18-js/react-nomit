import React, { Component } from 'react';
import './SocialIcons.css'

class SocialIcons extends Component{
constructor(props){
    super(props)
    this.state={likeStatus:this.props.like,commentStatus:this.props.comment,saveStatus:this.props.save}
}
isLiked(){
    this.setState({likeStatus:!this.state.likeStatus})
    // console.log(this.props.id+"a"+this.state.likeStatus+"a"+"like")
    this.props.update(this.props.id,'like',!this.state.likeStatus)
}
isCommented(){
    this.setState({commentStatus:!this.state.commentStatus})
    this.props.update(this.props.id,'comment',!this.state.commentStatus)
}
isSaved(){
    this.setState({saveStatus:!this.state.saveStatus})
    this.props.update(this.props.id,'save',!this.state.saveStatus)
}
render(){
    return(
        <div>
            <div className="icons">
            <a onClick={this.isLiked.bind(this)}>{this.state.likeStatus?<i className="fa fa-heart"></i>:<i className="fa fa-heart-o"></i>}</a>
            <a className="comment-icon"onClick={this.isCommented.bind(this)}><i className="fa fa-comment-o"></i></a>
            <a className="bookmark" onClick={this.isSaved.bind(this)}>{this.state.saveStatus?<i className="fa fa-bookmark"></i>:<i className="fa fa-bookmark-o"></i>}</a>
            </div>
        </div>
    )
}
}
export default SocialIcons;