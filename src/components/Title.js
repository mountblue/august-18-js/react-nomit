import React, { Component } from 'react';
import './Title.css'


class Title extends Component{
    render(){
        return(
            <div>
            <div className="profile">
                <img src={this.props.image}></img>
                <h5>{this.props.name}</h5>
            </div>
            </div>
        )
    }
}
export default Title;