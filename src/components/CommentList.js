import React, { Component } from 'react';
import './CommentList.css'
class CommentList extends Component{
    constructor(props)
    {
        super(props)
    }
    render()
    {
        let commentItems;
        if(this.props.status)
        {  
            commentItems=this.props.list.map((item,i)=>{
                return<li className="a"key={i}><label>{item.userName} </label>{item.comment}</li>
            })
        }
        else
        commentItems=[];
        return(
            <div>
                <div className="comment-box">
                <ul>
                {commentItems}
                </ul>
                </div>
            </div>
        )
    }
}
export default CommentList;